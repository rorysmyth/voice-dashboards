$(function(){

    function Bootstrap(){
        this.components = [];
        this.services = [];
        this.serviceTags = [];
        this.componentTags = [];
    }

    Bootstrap.prototype.addComponent = function (data) {
        this.components.push(data)
        return this;
    }

    Bootstrap.prototype.addService = function (data) {
        this.services.push(data)
        return this;
    }

    Bootstrap.prototype.bootStrap = function () {


        if (this.components.length == 0 && this.services.length == 0) {
            return;
        };

        var $this = this;

        $.each(this.components, function (index, component) {

            if ( component.controllers.length > 0 ) {
                $.each(component.controllers, function (index, controller) {
                    var template = "\<script src='/js/app/{{{dir}}}/controllers/{{{controller}}}.js'\>\</script\>";
                    var tag = template.replace("{{{dir}}}", component.dir).replace("{{{controller}}}", controller);
                    $this.componentTags += tag;
                });
            }

            if ( component.directives.length > 0 ) {
                $.each(component.directives, function (index, directive) {
                    var template = "\<script src='/js/app/{{{dir}}}/directives/{{{directive}}}.js'\>\</script\>";
                    var tag = template.replace("{{{dir}}}", component.dir).replace("{{{directive}}}", directive);
                    $this.componentTags += tag;
                });
            }

        });

        if (this.services.length > 0) {
            $.each(this.services[0], function (index, service) {
                var template = "\<script src='/js/app/services/{{{service}}}.js'\>\</script\>";
                var tag = template.replace("{{{service}}}", service);
                $this.serviceTags += tag;
            });
        };

        $('body')
            .append(this.serviceTags)
            .append(this.componentTags);

    }

    var app = new Bootstrap;
    app
        .addService([

        ])
        .addComponent({
            dir:'general',
            controllers: ['general'],
            directives: []
        })
        .addComponent({
            dir:'current-queue',
            controllers: ['current-queue'],
            directives: ['sticky']
        })
        .addComponent({
            dir:'queue-graph',
            controllers: [],
            directives: ['queue-graph']
        })
        .addComponent({
            dir:'agent-availability',
            controllers: [],
            directives: ['agent-availability']
        })
        .addComponent({
            dir:'benchmarks',
            controllers: [],
            directives: ['benchmarks']
        })
        .addComponent({
            dir:'sparklines',
            controllers: ['sparklines'],
            directives: ['sparklines']
        })
        .addComponent({
            dir:'live-queue',
            controllers: ['live-queue'],
            directives: []
        })
        .addComponent({
            dir:'lotus',
            controllers: ['lotus'],
            directives: []
        })
        .addComponent({
            dir:'agents',
            controllers: ['agents'],
            directives: ['popover-table','status-dropdown']
        })
        .addComponent({
            dir:'agent-table-popover',
            controllers: [],
            directives: ['agent-table-popover']
        })
        .addComponent({
            dir:'agent-filter-popover',
            controllers: [],
            directives: ['agent-filter-popover']
        })
        .addComponent({
            dir:'toggle',
            controllers: [],
            directives: ['toggle']
        })
        .bootStrap();


});