app.directive('agentTablePopover', function($document){
    return {
        templateUrl:'/js/app/agent-table-popover/templates/agent-table-popover.html',
        restrict:'E',
        scope: {
            agents : '='
        },
        link: function($scope, ele, attr, controller){

            $scope.agentsCtrl = $scope.agents;

            $scope.popover = {
                active: false
            };
            
            $document.bind('mousedown', function(e){

                var clicked = e.toElement;
                var parent = ele[0];

                if(!parent.contains(clicked)){
                    $scope.popover  = false;
                }

            })
            
        }
    }
});