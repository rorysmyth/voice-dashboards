app.directive('toggle', function(){
	return {
		restrict: 'E',
		scope: {
			model: "=model",
			option: "=option"
		},
		templateUrl: '/js/app/toggle/templates/toggle.html',
		controller: function($scope,$log){

			$scope.toggleValue = function(){
				$scope.model[$scope.option] = !$scope.model[$scope.option]
			};
			
			$scope.value = function(){
				return $scope.model[$scope.option] === true;
			};

		}
	}
})