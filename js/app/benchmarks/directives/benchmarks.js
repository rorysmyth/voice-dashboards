app.directive('benchmarks', function($log){
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, ele, attr, controller){


            ele.highcharts({
                chart: {
                    type: 'bar'
                },
                title:false,
                xAxis: {
                    categories: ['Avg wrap up time', 'Avg wait time', 'Longest wait time'],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Time (seconds)',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' seconds'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Your Zendesk',
                    data: [183, 23, 44]
                }, {
                    name: 'Global average',
                    data: [161, 33, 65]
                }]
            })

        }
    }
});