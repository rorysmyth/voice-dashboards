app.controller('generalController', ['$scope', function ($scope) {

    $scope.metricList = [
        {
            name: "Total calls",
            value: "totalCalls",
            label: 'calls'
        },
        {
            name: "Average wrap up time",
            value: "avgWrapUpTime",
            label: 'seconds'
        },
        {
            name: "Abandoned",
            value: "abandoned",
            label: 'calls'
        },
        {
            name: "Average wait time",
            value: "avgWaitTime",
            label: 'seconds'
        },
        {
            name: "Longest wait time",
            value: "longestWaitTime",
            label: 'seconds'
        },
        {
            name: "Exceeded wait time",
            value: "exceededWaitTime",
            label: 'calls'
        },
        {
            name: "Most calls waiting",
            value: "mostCallsWaiting",
            label: 'calls'
        },
        {
            name: "Voicemail selected",
            value: "voicemailSelected",
            label: 'calls'
        },
        {
            name: "Routed to voicemail",
            value: "routedToVoicemail",
            label: 'calls'
        }
    ];
    

    $scope.concatNumber = [];
    $scope.concatAverages = {};
    _.each($scope.metricList, function(metric, key){
        $scope.concatAverages[metric.value] = {
            name: metric.name,
            value: 0
        }
    });


    this.numberData = [
        {
            name: "Dublin office",
            number: "+353 1223344",
            locale: 'ie',
            queueWindow: {
                totalCalls: [31, 22, 45, 30, 44, 41, 34, 25, 39, 40, 33, 26, 29, 46, 24, 37, 36, 28, 47, 23, 43, 49, 35, 42],
                avgWrapUpTime: [107, 139, 101, 149, 117, 60, 119, 96, 169, 171, 125, 77, 74, 97, 158, 178, 105, 111, 130, 83, 138, 159, 112, 172],
                abandoned: [8, 7, 1, 10, 2, 9, 10, 9, 2, 1, 7, 4, 9, 1, 10, 7, 4, 8, 8, 4, 1, 7, 5, 8],
                avgWaitTime: [21, 31, 37, 23, 37, 43, 16, 41, 32, 17, 51, 38, 36, 38, 18, 20, 21, 34, 39, 39, 36, 55, 16, 32],
                longestWaitTime: [39, 71, 48, 81, 78, 80, 55, 68, 57, 53, 72, 71, 68, 79, 69, 39, 70, 67, 34, 65, 39, 45, 46, 55],
                exceededWaitTime: [8, 6, 9, 4, 5, 2, 10, 7, 6, 5, 3, 6, 9, 4, 1, 5, 8, 4, 6, 8, 0, 8, 2, 10],
                mostCallsWaiting: [0, 3, 4, 1, 2, 4, 5, 2, 5, 2, 2, 0, 5, 4, 3, 3, 5, 0, 4, 5, 1, 2, 3, 2],
                voicemailSelected: [3, 5, 1, 2, 2, 2, 0, 5, 2, 1, 5, 1, 1, 2, 1, 1, 3, 0, 4, 2, 5, 1, 5, 3],
                routedToVoicemail: [6, 0, 5, 12, 12, 0, 0, 7, 0, 7, 13, 15, 15, 6, 13, 7, 7, 8, 3, 6, 2, 7, 3, 0]
            },
            ticked: true
        }, {
            name: "New York office",
            number: "+1 555 222 345",
            locale: 'us',
            queueWindow: {
                totalCalls: [80, 48, 42, 72, 101, 58, 147, 54, 59, 144, 49, 45, 43, 95, 74, 84, 107, 44, 130, 136, 141, 79, 97, 64],
                avgWrapUpTime: [140, 89, 80, 67, 82, 73, 173, 126, 177, 111, 169, 75, 77, 105, 127, 178, 90, 114, 63, 158, 139, 137, 125, 156],
                abandoned: [1, 2, 5, 4, 5, 4, 4, 3, 4, 2, 5, 2, 1, 4, 3, 3, 4, 4, 3, 2, 1, 4, 3, 2],
                avgWaitTime: [21, 31, 37, 23, 37, 43, 16, 41, 32, 17, 51, 38, 36, 38, 18, 20, 21, 34, 39, 39, 36, 55, 16, 32],
                longestWaitTime: [49, 77, 43, 46, 46, 64, 47, 40, 56, 72, 74, 46, 31, 40, 68, 32, 83, 40, 31, 43, 71, 39, 50, 59],
                exceededWaitTime: [5, 3, 10, 10, 6, 5, 4, 9, 7, 1, 3, 8, 7, 5, 1, 10, 6, 0, 1, 6, 1, 5, 0, 0],
                mostCallsWaiting: [1, 5, 4, 3, 0, 2, 3, 4, 1, 1, 4, 3, 1, 2, 4, 2, 4, 0, 5, 2, 4, 2, 0, 2],
                voicemailSelected: [0, 4, 4, 5, 0, 1, 0, 3, 3, 5, 4, 4, 3, 2, 1, 0, 5, 0, 1, 4, 5, 0, 3, 4],
                routedToVoicemail: [10, 1, 8, 12, 2, 11, 8, 4, 4, 0, 8, 5, 6, 11, 12, 12, 6, 0, 12, 14, 5, 2, 11, 7]
            },
            ticked: true
        }, {
            name: "London office",
            number: "+44 123 456 7",
            locale: 'uk',
            queueWindow: {
                totalCalls: [85, 74, 59, 87, 49, 48, 58, 67, 35, 47, 75, 80, 60, 45, 88, 34, 66, 83, 76, 37, 89, 38, 70, 63],
                avgWrapUpTime: [140, 89, 80, 67, 82, 73, 173, 126, 177, 111, 169, 75, 77, 105, 127, 178, 90, 114, 63, 158, 139, 137, 125, 156],
                abandoned: [1, 2, 5, 4, 5, 4, 4, 3, 4, 2, 5, 2, 1, 4, 3, 3, 4, 4, 3, 2, 1, 4, 3, 2],
                avgWaitTime: [21, 31, 37, 23, 37, 43, 16, 41, 32, 17, 51, 38, 36, 38, 18, 20, 21, 34, 39, 39, 36, 55, 16, 32],
                longestWaitTime: [49, 77, 43, 46, 46, 64, 47, 40, 56, 72, 74, 46, 31, 40, 68, 32, 83, 40, 31, 43, 71, 39, 50, 59],
                exceededWaitTime: [0, 8, 4, 3, 6, 5, 10, 10, 4, 6, 5, 5, 4, 10, 5, 6, 2, 4, 3, 1, 5, 2, 5, 2],
                mostCallsWaiting: [0, 3, 3, 0, 5, 5, 2, 5, 3, 5, 5, 4, 2, 4, 0, 3, 1, 1, 5, 2, 4, 3, 1, 4],
                voicemailSelected: [4, 3, 2, 1, 5, 4, 4, 2, 3, 0, 5, 4, 0, 5, 3, 5, 1, 3, 2, 0, 5, 4, 1, 2],
                routedToVoicemail: [10, 8, 14, 7, 3, 2, 3, 11, 6, 7, 12, 9, 10, 10, 7, 2, 10, 7, 9, 11, 12, 12, 4, 8]
            },
            ticked: true
        }
    ]


    $scope.$watch(
        angular.bind(this, function () {
            return this.concatNumber;
        }),
        function (newVal, oldVal) {

            if (!_.isEmpty(oldVal) && !_.isEmpty(newVal)) {
                $scope.buildAverages(newVal);
                $scope.buildTimeline(newVal);
            }

        });


    $scope.buildTimeline = function (numbers) {

        var ending = true,
            timelineMetric = {};

        _.each($scope.metricList, function(metric, key){
            timelineMetric[metric.value] = [];
        });

        // timeline for number averages
        _.each(numbers, function (number) {
            _.each(timelineMetric, function (value, key) {
                timelineMetric[key].push(number.queueWindow[key]);
            });
        });

        //as each hour has an array of each of the number values, we need to condense this down to one
        _.each(timelineMetric, function (value, key) {
            $scope.concatAverages[key].timeline = _.map(_.zip.apply(_, timelineMetric[key]), function (pieces) {
                return _.reduce(pieces, function (a, b) {
                    return a + b;
                }, 0);
            });
        });

        // give the metric the "calls" or "seconds" label
        _.each(timelineMetric, function(value,key){
            var metric = _.where($scope.metricList, {value: key})[0];
            $scope.concatAverages[key].label = metric.label;
        });

        if(ending){
            _.each($scope.concatAverages, function(value, key){
                var cut = 18;
                for(cut; cut < 24; cut++){
                    $scope.concatAverages[key].timeline[cut] = null
                }
            })
        }
        
    };

    $scope.buildAverages = function (numbers) {

        var sparklineAverages = {};

        _.each($scope.metricList, function(metric, key){
            sparklineAverages[metric.value] = 0;
        });

        
        _.each(numbers, function (number) {
            _.each(sparklineAverages, function (value, key) {
                // total value for sparklines
                sparklineAverages[key] += _.reduce(number.queueWindow[key], function (memo, num) {
                    return memo + num
                }, 0);
            });
        });


        // give the metric the "calls" or "seconds" label
        _.each(sparklineAverages, function(value,key){
            var metric = _.where($scope.metricList, {value: key})[0];
            $scope.concatAverages[key].slug = metric.value;
        });

        // add to the average object for metrics
        if (numbers.length > 0) {
            $scope.concatAverages.avgWrapUpTime.value = Math.ceil((sparklineAverages.avgWrapUpTime / 24) / numbers.length);
            $scope.concatAverages.avgWaitTime.value = Math.ceil((sparklineAverages.avgWaitTime / 24) / numbers.length);
            $scope.concatAverages.longestWaitTime.value = Math.ceil((sparklineAverages.longestWaitTime / 24) / numbers.length);
        } else {
            $scope.concatAverages.avgWrapUpTime.value = Math.ceil(sparklineAverages.avgWrapUpTime / 24);
            $scope.concatAverages.avgWaitTime.value = Math.ceil(sparklineAverages.avgWaitTime / 24);
            $scope.concatAverages.longestWaitTime.value = Math.ceil(sparklineAverages.longestWaitTime / 24);
        }
        $scope.concatAverages.totalCalls.value = sparklineAverages.totalCalls;
        $scope.concatAverages.abandoned.value = sparklineAverages.abandoned;
        $scope.concatAverages.exceededWaitTime.value = sparklineAverages.exceededWaitTime;
        $scope.concatAverages.mostCallsWaiting.value = sparklineAverages.mostCallsWaiting;
        $scope.concatAverages.voicemailSelected.value = sparklineAverages.voicemailSelected;
        $scope.concatAverages.routedToVoicemail.value = sparklineAverages.routedToVoicemail;

    };

    //  template for the dropdown
    var metrics = _.map($scope.metricList, function(metric, key){
        var x = {};
        x.name = metric.name;
        x.value = metric.value;
        x.ticked = false;
        return x;
    });

    this.metricSelectionOne = angular.copy(metrics);
    this.metricSelectionOne[0].ticked = true;
    this.metricSelectionTwo = angular.copy(metrics);
    this.metricOne = [];
    this.metricTwo = [];
    this.concatNumber = $scope.concatNumber;
    this.concatAverages = $scope.concatAverages;

    this.resetSelection = function(){
        var checkedItem = _.where(this.metricSelectionTwo, {ticked:true});
        checkedItem[0].ticked = false;
    };

    this.sparklineSelection = function(metric){
        if(_.isEmpty(this.metricTwo)){
            //no second one selected, just populate the first
            var currentSelected = _.where(this.metricSelectionOne, {ticked:true});
            if(currentSelected[0].value == metric){
                return;
            }else {
                currentSelected[0].ticked = false;
                var newSelected = _.where(this.metricSelectionOne, {value:metric});
                newSelected[0].ticked = true;
            }
        } else if (!_.isEmpty(this.metricTwo)){
            //no second one selected, just populate the first
            var currentSelected = _.where(this.metricSelectionTwo, {ticked:true});
            if(currentSelected[0].value == metric){
                return;
            }else {
                currentSelected[0].ticked = false;
                var newSelected = _.where(this.metricSelectionTwo, {value:metric});
                newSelected[0].ticked = true;
            }

        }
    }

    this.twoMetrics = function(){
        return !_.isEmpty(this.metricTwo[0]);
    }

    Highcharts.theme = {
        colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
            "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
        chart: {
            backgroundColor: null,
            style: {
                fontFamily: "Proxima Nova"
            }
        },
        title: {
            style: {
                fontSize: '16px',
                fontWeight: 'bold',
                marginTop: 20
            }
        },
        tooltip: {
            borderWidth: 0,
            backgroundColor: 'rgba(255,255,255,1)',
            shadow: true
        },
        legend: {
            itemStyle: {
                fontWeight: 'bold',
                fontSize: '13px'
            }
        },
        xAxis: {
            labels: {
                style: {
                    fontSize: '12px'
                }
            }
        },
        yAxis: {
            gridLineColor: '#dddddd'
        },
        plotOptions: {
            series: {
                fillOpacity: 0.1
            }
        },


        // General
        background2: '#F0F0EA'

    };

    Highcharts.setOptions(Highcharts.theme);


}]);