app.directive('sticky', function($window){
    return {
        restrict:'A',
        link: function($scope,ele,attr,controller){

            $scope.yPos = function(){
                var scrollTop     = angular.element($window).scrollTop(),
                    elementOffset = angular.element(ele).offset().top,
                    distance      = (elementOffset - scrollTop);
                return distance;
            };

            angular.element($window).bind("scroll", function(e){
                if($scope.yPos() < 20){
                    ele.addClass('sticky')
                }

                if(angular.element($window).scrollTop() < 120){
                    ele.removeClass('sticky')
                }

            });


        }
    }
})