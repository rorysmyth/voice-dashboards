app.controller('currentQueueController', function($scope, $interval){

    $scope.queueActivity = [

        {   name:"calls waiting in queue",
            value: 2,
            time:false,
            green:false,
            down: true,
            percentage:0
        },
        {   name:"agents online",
            value: 150,
            time: false,
            green:true,
            down: true,
            percentage:0
        },
        {   name:"average wait time",
            value: 31,
            time:true,
            green:true,
            down: true,
            percentage:0
        },
        {   name:"longest wait time",
            value: 62,
            time: true,
            green:true,
            down: true,
            percentage:0
        }

    ];

    $interval(function(){


        $x = 0;
        for($x; $x < $scope.queueActivity.length; $x++){

            var metric = $scope.queueActivity[$x];
            var oldVal = metric.value;

            var adjustment = _.random(1,10);
            var potentialValues = [metric.value + adjustment, metric.value - adjustment];

            if(potentialValues[0] > 0 && potentialValues[1] > 0){
                metric.value = potentialValues[_.random(0,1)];
            }

            if(potentialValues[0] > 0 && potentialValues[1] < 0){
                metric.value = potentialValues[0];
            }

            if(potentialValues[0] < 0 && potentialValues[1] > 0){
                metric.value = potentialValues[1];
            }

            var newVal = metric.value;

            if(newVal > oldVal){

                metric.percentage = Math.floor((100-((oldVal/newVal) * 100)));
                metric.down = false;
                switch (metric.name) {
                    case 'agents online':
                        metric.green = true;
                        break;
                    case 'calls waiting in queue':
                        metric.green = false;
                        break;
                    case 'average wait time':
                        metric.green = false;
                        break;
                    case 'longest wait time':
                        metric.green = false;
                        break;
                }

            } else {

                metric.percentage = Math.floor((100-((newVal/oldVal) * 100)));
                metric.down = true;

                switch (metric.name) {
                    case 'agents online':
                        metric.green = false;
                        break;
                    case 'calls waiting in queue':
                        metric.green = true;
                        break;
                    case 'average wait time':
                        metric.green = true;
                        break;
                    case 'longest wait time':
                        metric.green = true;
                        break;
                }
            }

        }

    },5000)


});