app.directive('agentFilterPopover', function($document){
    return {
        templateUrl:'/js/app/agent-filter-popover/templates/agent-filter-popover.html',
        replace:false,
        restrict:'E',
        scope: {
            agents : '='
        },
        link: function($scope, ele, attr, controller){

            $scope.agentsCtrl = $scope.agents;

            $scope.popover = {
                active: false
            };
            
            $document.bind('mousedown', function(e){

                var clicked = e.toElement;
                var parent = ele[0];

                if(!parent.contains(clicked)){
                    $scope.popover  = false;
                }


            })
            
        }
    }
});