app.directive('popoverTable', function($document){
    return {
        templateUrl: '/js/app/agents/templates/popover-table.html',
        replace:true,
        scope: {
            agent: "=",
            active: "=",
            agents: "="
        },
        link: function($scope, ele, attr, controller){

            console.log($scope.agents);
            
            $document.bind('mousedown', function(e){

                var clicked = e.toElement;
                var parent = ele[0];

                if(!parent.contains(clicked)){
                    $scope.agents.detail  = null;
                }

            })
            
        }
    }
})