app.controller('agentsController', function($scope){

    var name = ["Oneal Sloan","Blanchard Hopper","Mccray Leach","Fuentes Mcclain","Beck Holcomb","Roberta Potter","Amanda Hopkins","Vang Dillard","Kristine Collins","Hallie Chavez","Judy Schwartz","Holden Franks","Penny Gregory","Sara Buck","Eula Mayer","Lana Haynes","Fletcher Meyers","Holman Townsend","Phelps Lee","Erika Todd","Kellie Lewis","Compton Mclaughlin","Green Vazquez","Washington Silva","Hunter Becker","Price Hardy","Bridget Hess","Cash Rutledge","Winifred Henson","Maynard Bowen","Gretchen Hurley","Conley Sandoval","Patti Rivers","Gonzales Melton","Leanna Wolf","Helga Powell","Ursula Raymond","Reynolds Hyde","Erna Bruce","Elizabeth Mendez","Donovan Hunter","Belinda Walker","Horn Nieves"];
    var status = [
        "in call",
        "unavailable",
        "available"
    ];

    this.agents = [];
    this.predicate = "name";
    this.group = false;
    this.detail = null;

    this.comparison = false;
    this.reverse = false;

    this.setPredicate = function(predicate){
        console.log(predicate);
        this.predicate = predicate;
        this.reverse = !this.reverse
    };

    this.makeAgents = function(){
        for(x = 0; x < 10; x++){
            var agent = {
                name : name[_.random(0,40)],
                status : status[_.random(0,2)],
                avatar: _.random(1,10),
                timeAvailable: Math.floor(_.random(240,300)),
                avgTalkTime: Math.floor(_.random(30,180)),
                callsAccepted: Math.floor(_.random(0,20)),
                avgWrapUpTime: Math.floor(_.random(30,300)),
                callsDenied: Math.floor(_.random(1,15)),
                callsMissed: Math.floor(_.random(1,15)),
                totalTalkTime: Math.floor(_.random(120,500)),
                totalWrapUpTime: Math.floor(_.random(60,120)),
                show:false
            };
            this.agents.push(agent);
        }
    };

    if(_.isEmpty($scope.agents)){
        this.makeAgents()
    }

    this.toggleDetail = function(agent){
        if(this.detail == agent){
            this.detail = null
        } else {
            this.detail = agent;
        }
    };

    this.isActive = function(agent){
        return agent === this.detail;
    }

    this.groups = [
        {
            name: "in call",
            agents: _.where(this.agents,{status:"in call"})
        },
        {
            name: "available",
            agents: _.where(this.agents,{status:"available"})
        },
        {
            name: "unavailable",
            agents: _.where(this.agents,{status:"unavailable"})
        }
    ];

    this.sort = function(){
        this.group = (this.groupOption == "") ? false : true;
    };

    console.log(this.groups.length);

});