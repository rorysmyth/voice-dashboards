app.directive('queueGraph', function($log, $timeout){
    return {
        restrict: 'A',
        replace: false,
        scope: {
            averages: "=",
            series: "="
        },
        link: function($scope, ele, attr, controller){
            

            $scope.draw = function(){

                var series = angular.copy($scope.averages[$scope.series[0][0].value]);
                ele.highcharts({
                    chart: {
                        zoomType: 'x',
                        type: 'area'
                    },
                    title: {
                        text: 'Compare key metrics for Zendesk Voice'
                    },
                    plotOptions: {
                        column: {
                            fillOpacity: 0.2
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    tooltip: {
                        shared: true,
                        crosshairs: true
                    },
                    xAxis: [{
                        categories: ['12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00']
                    }],
                    yAxis: [{
                        title: {
                            text: series.name,
                            style: {
                                color:Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color:Highcharts.getOptions().colors[0]
                            }
                        }
                    }],
                    series: [{
                        name: series.name,
                        data: series.timeline,
                        tooltip: {
                            valueSuffix: ' ' + series.label
                        }

                    }]
                });
                $scope.chart = ele.highcharts()

            };

            /*
            redraw the graph
             */
            $scope.redraw = function(dropdownVal){

                console.log("recieving");

                // adding second series
                if(_.isEmpty(dropdownVal.old.second) && !_.isEmpty(dropdownVal.new.second)){

                    console.log("adding second metric");
                    $scope.addMetric(1);
                    return;

                }else if(!_.isEmpty(dropdownVal.old.second) && _.isEmpty(dropdownVal.new.second) ){

                    console.log("removeing second metric");
                    $scope.removeMetric(1);
                    return;

                }

                // if there's a second series there

                if(!_.isEmpty(dropdownVal.old.second) && !_.isEmpty(dropdownVal.new.second) && dropdownVal.old.first.value == dropdownVal.new.first.value){

                    console.log("updating second metric");
                    $scope.updateMetric(1)

                } else if(!_.isEmpty(dropdownVal.new.second) && dropdownVal.old.second.value == dropdownVal.new.second.value){

                    console.log("updating first metric when second metric is false");
                    $scope.updateMetric(0)

                } else if(_.isEmpty(dropdownVal.new.second) && dropdownVal.old.first.value != dropdownVal.new.first.value) {

                    console.log("updating first metric when second metric is true");
                    $scope.updateMetric(0)

                }

            };

            $scope.addMetric = function(pos){
                var model = $scope.buildModel(pos);
                    $scope.addAxis(model);
                    $scope.addSeries(pos, model);
            };
                $scope.updateMetric = function(pos){
                    var model = $scope.buildModel(pos);
                    $scope.updateAxis(pos,model);
                    $scope.updateSeries(pos, model);
                };
                $scope.removeMetric = function(pos){
                    $scope.removeSeries(pos);
                    $scope.removeAxis(pos);
                };

            $scope.buildModel = function(pos){
                // make the model
                var model = {
                    title: $scope.series[pos][0].name,
                    value: $scope.series[pos][0].value,
                    label: angular.copy($scope.averages[$scope.series[pos][0].value].label),
                    timeline: angular.copy($scope.averages[$scope.series[pos][0].value].timeline)
                };
                return model;
            };

            $scope.addSeries = function(pos, model){
                // plot the series data
                $scope.chart.addSeries({
                    name: model.title,
                    type: 'area',
                    yAxis: model.value,
                    data: model.timeline,
                    color:'rgb(247, 163, 92)',
                    tooltip: {
                        valueSuffix: ' ' + model.label
                    }
                });
            };
                $scope.updateSeries = function(pos,model){
                    $scope.chart.series[pos].update({
                        name:model.title,
                        tooltip: {
                            valueSuffix: ' ' + model.label
                        }
                    });
                    $scope.chart.series[pos].setData(model.timeline);
                };
                $scope.removeSeries = function(pos){
                    $scope.chart.series[pos].remove();
                };

            $scope.addAxis = function(model){
                // add the new axis
                $scope.chart.addAxis({
                    id: model.value,
                    title: {
                        text: model.title,
                        style: {
                            color:Highcharts.getOptions().colors[1]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color:Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true
                });

            };
                $scope.updateAxis = function(pos,model){
                    $scope.chart.series[pos].update({
                        name:model.title
                    });
                    $scope.chart.yAxis[pos].update({
                        title: {
                            text: model.title
                        },
                        labels: {
                            format: '{value}'
                        }
                    })
                };
                $scope.removeAxis = function(pos){
                    $scope.chart.yAxis[pos].remove();
                };

            
            /*  
            watch changes to the 
             */
            $scope.$watch(function(){
                
                return $scope.averages
                
            }, function(newVal, oldVal){

                return $timeout(function(){
                    var series = {
                        old: {
                            first: $scope.series[0],
                            second: $scope.series[1]
                        },
                        new: {
                            first: $scope.series[0],
                            second: $scope.series[1]
                        }
                    };
                    $scope.redraw(series)
                },100);
                
            },true );

            /*
            watch for changes to the dropdowns
             */
            $scope.$watch(function(){

                return $scope.series;

            }, function(newSeries, oldSeries){

                var series = {
                    old: {
                        first: oldSeries[0][0],
                        second: oldSeries[1][0]
                    },
                    new: {
                        first: newSeries[0][0],
                        second: newSeries[1][0]
                    }
                };

                if(_.isEmpty(series.old.first) && _.isEmpty(series.old.second) && !_.isEmpty(series.new.first) && !_.isEmpty(series.new.first)){
                    return $timeout(function(){
                        $scope.draw()
                    },300)
                }
                
                if(!_.isEmpty($scope.chart)){
                    return $timeout(function(){
                        console.group("redrawing")
                        console.log("passing in:");
                        console.log(series);
                        $scope.redraw(series)
                        console.groupEnd()
                    },100);
                }
                
            },true);


        }
    }
});