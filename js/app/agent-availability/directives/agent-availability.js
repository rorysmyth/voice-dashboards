app.directive('agentAvailability', function($log){
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, ele, attr, controller){


            ele.highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: false,
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y} agents</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y} agents',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                        showInLegend: true
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    type: 'pie',
                    name: 'Agent availability',
                    data: [
                        {
                            name: 'Available',
                            y: 40,
                            sliced: true,
                            selected: true
                        },
                        ['In call',    20],
                        ['Wrap up',     15],
                        ['Unavailable',   25],
                    ]
                }]
            })

        }
    }
});