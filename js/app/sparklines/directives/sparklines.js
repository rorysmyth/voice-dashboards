app.directive('sparkline', function($log){
    return {
        restrict: 'A',
        replace: false,
        scope: {
            metric: "="
        },
        link: function($scope, ele, attr, controller){

            $scope.$watch(function(){
                return $scope.metric;
            }, function(newVal, oldVal){

                if(oldVal.value == 0 && newVal.value != 0){
                    //first time init
                    $scope.draw();
                }

                if(oldVal.value != 0 && newVal.value != 0 ){
                    $scope.redraw();
                }

            },true)


            $scope.draw = function(){

                ele.highcharts('SparkLine', {
                    series: [{
                        //data: $scope.data,
                        data: [
                            $scope.metric.timeline[0],
                            $scope.metric.timeline[6],
                            $scope.metric.timeline[12],
                            $scope.metric.timeline[18],
                            $scope.metric.timeline[24],
                        ]
                    }],
                    xAxis:{
                        categories:['12:00','18:00','23:00','06:00',"12:00"]
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size: 10px">' + '{point.x}:</span><br/>',
                        pointFormat: '<b>{point.y}</b>'
                    },
                    click: function(e) {

                    }
                });

                $scope.chart = ele.highcharts()

            }

            $scope.redraw = function(){
                $scope.chart.series[0].setData(
                [
                    $scope.metric.timeline[0],
                    $scope.metric.timeline[6],
                    $scope.metric.timeline[12],
                    $scope.metric.timeline[18],
                    $scope.metric.timeline[24]
                ])
            };

            Highcharts.SparkLine = function (options, callback) {
                var defaultOptions = {
                    chart: {
                        renderTo: (options.chart && options.chart.renderTo) || this,
                        backgroundColor: null,
                        borderWidth: 0,
                        type: 'area',
                        margin: [2, 0, 2, 0],
                        width: 60,
                        height: 20,
                        style: {
                            overflow: 'visible'
                        },
                        skipClone: true
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        startOnTick: false,
                        endOnTick: false,
                        tickPositions: []
                    },
                    yAxis: {
                        endOnTick: false,
                        startOnTick: false,
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        },
                        tickPositions: [0]
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        backgroundColor: null,
                        borderWidth: 0,
                        shadow: false,
                        useHTML: true,
                        hideDelay: 0,
                        shared: true,
                        padding: 0,
                        positioner: function (w, h, point) {
                            return { x: point.plotX - w / 2, y: point.plotY - h};
                        }
                    },
                    plotOptions: {
                        series: {
                            animation: false,
                            lineWidth: 1,
                            shadow: false,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            marker: {
                                radius: 1,
                                states: {
                                    hover: {
                                        radius: 2
                                    }
                                }
                            },
                            fillOpacity: 0.25
                        },
                        column: {
                            negativeColor: '#910000',
                            borderColor: 'silver'
                        }
                    }
                };
                options = Highcharts.merge(defaultOptions, options);

                return new Highcharts.Chart(options, callback);
            }

            ele.bind('mousedown', function () {

            });


        }
    }
});