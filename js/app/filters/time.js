angular.module('dashboardFilters',[])
    .filter('time',function(){
        return function(seconds){

            var totalSec = seconds;
            var hours = parseInt( totalSec / 3600 ) % 24;
            var minutes = parseInt( totalSec / 60 ) % 60;
            var seconds = parseInt(totalSec % 60, 10);
            var result = (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

            return result;

        }
    })

