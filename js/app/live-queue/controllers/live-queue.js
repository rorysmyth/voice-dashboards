app.controller('liveQueueController', function($scope, $interval, $timeout){

    var randomNumber = function(){
        var prefixes = ['+1','+44','+32','+65'];
        var number = {
            prefix : prefixes[_.random(0,3)],
            start: _.random(30,555),
            mid: _.random(30,200),
            end: _.random(1,9)
        };
        return number.prefix+' '+number.start+' '+number.mid+' '+number.end;
    }

    $scope.calls = [
        {
            callerId: randomNumber(),
            callerLocale: 'ie',
            destination: 'Dublin office',
            destinationLocale: 'ie',
            timeInQueue: 5,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'us',
            destination: 'New York Office',
            destinationLocale: 'us',
            timeInQueue: 8,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'ie',
            destination: 'New York Office',
            destinationLocale: 'us',
            timeInQueue: 16,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'us',
            destination: 'New York Office',
            destinationLocale: 'us',
            timeInQueue: 19,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'us',
            destination: 'San Francisco Office',
            destinationLocale: 'us',
            timeInQueue: 32,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'ie',
            destination: 'Dublin Office',
            destinationLocale: 'ie',
            timeInQueue: 45,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'ie',
            destination: 'London office',
            destinationLocale: 'uk',
            timeInQueue: 50,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'ie',
            destination: 'Dublin Office',
            destinationLocale: 'ie',
            timeInQueue: 52,
            status:'queued'
        },{
            callerId: randomNumber(),
            callerLocale: 'us',
            destination: 'San Francisco Office',
            destinationLocale: 'us',
            timeInQueue: 59,
            status:'queued'
        }
    ]

    console.log($scope.calls);

    $scope.predicate = '';

    $scope.connectFirstCall = function(){

        $scope.calls[0].status = 'routing';

        var copy = angular.copy($scope.calls[0])
        copy.status = 'queued';
        copy.timeInQueue = 0;

        $timeout(function(){
            $scope.calls[0].status = 'routed';
        },2000)
            .then(function(){
                return $timeout(function(){
                    $scope.calls.splice(0,1)
                    $scope.calls.push(copy)
                },1000)
            });

        $scope.calls[0].status = 'routing';

    }

    $scope.decreaseTime = function(){
        _.each($scope.calls,function(val, key, list){
            $scope.calls[key].timeInQueue += 1;
        })
    }

    $interval($scope.connectFirstCall, _.random(3000,4000));

    $interval($scope.decreaseTime,1000)

});